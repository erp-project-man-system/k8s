# Practice environment

# Content
I am using this repo as practice environment for the project. All the demo projects that can add value to the current or future development, must be added here to avoid missing them at some point.

# Demo_01-Ingress_HTTPS
In this demo, I have created a test kubernetes cluster deployed to GKE consisting of deployment of test web app, exposed by a service of type ClusterIP and serve to the world using Ingress with default GKE controller. The Ingress is configured with HTTPS and subdomain, in this case: `test.project-start.online`. To have a secure HTTPS connection, I am utilizing Signed SSL certificated from Let's Ecnrypt. Since they last only 90 days, I am using cert-manager to periodically update my SSL certificates so that the my test web app is never lacking security. Go checkout the configuration.